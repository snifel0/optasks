# optasks

This project is is to solve a specific problem which is important to me.

# Problem

There are many I/O-bound tasks which occupies certain amount of shared resource and release resource when it is done.

The goal is to run as many threads as possible with limited resource.