#!/usr/bin/env python
# coding: utf-8

from threading import Condition, Lock, Thread
from time import sleep, time


class Balance:
    """
    Shared balance between threads.
    """
    
    def __init__(self, initial_resource):
        self.c = Condition()
        self.resource = initial_resource

    def acquire(self, needed):
        self.c.acquire()
        while self.resource < needed:
            self.c.wait()
        self.resource -= needed
        self.c.release()
        return

    def release(self, returned, name):
        self.c.acquire()
        self.resource += returned
        self.c.notify_all()
        self.c.release()
        return



if __name__ == '__main__':

    start_time = time()
    b = Balance(5)
    p = Lock()

    class Thread_A(Thread):
        def __init__(self, name):
            super(Thread_A, self).__init__()
            self.name = name

        def run(self):
            global b
            b.acquire(1)
            p.acquire()
            print '{}: starts'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)
            p.release()
            sleep(1)
            p.acquire()
            print '{}: finished'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)
            p.release()
            b.release(1, self.name)

    class Thread_B(Thread):
        def __init__(self, name):
            super(Thread_B, self).__init__()
            self.name = name

        def run(self):
            global b
            b.acquire(2)
            p.acquire()
            print '{}: starts'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)
            p.release()
            sleep(2)
            p.acquire()
            print '{}: finished'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)        
            p.release()
            b.release(2, self.name)

    class Thread_C(Thread):
        def __init__(self, name):
            super(Thread_C, self).__init__()
            self.name = name

        def run(self):
            global b
            b.acquire(3)
            p.acquire()
            print '{}: starts'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)
            p.release()
            sleep(1)
            p.acquire()
            print '{}: finished'.format(self.name)
            print "--- %s seconds ---" % (time() - start_time)        
            p.release()
            b.release(3, self.name)
            
    tasks = [Thread_A('A1'), Thread_A('A2'), Thread_A('A3'),
             Thread_B('B1'), Thread_B('B2'), Thread_B('B3'),
             Thread_C('C1'), Thread_C('C2'), Thread_C('C3')]

    for t in tasks:
        t.start()

    for t in tasks:
        t.join()
